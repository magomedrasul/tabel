<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Records */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Все Записи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="records-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'user_id',
            'fio',
            'object',
            'created_at:datetime',
        ],
    ]) ?>

</div>
