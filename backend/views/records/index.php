<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use common\models\Records;
use nepster\basis\helpers\DateTimeHelper;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\RecordsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Все Записи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="records-index">

    <?php \yii\widgets\Pjax::begin()?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute'=>'user_id',
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Поиск по id пользователя'
                ]

            ],
            [
                'attribute'=>'fio',
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Поиск по ФИО'
                ]

            ],
            [
                'attribute'=>'object',
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Поиск по объекты'
                ]

            ],
            'arrived:datetime',
            'departed:datetime',
            [
                'attribute'=>'time',
                'label' => 'Время на объекте',
                 'value'=>function($model){

                     if ($model->departed) {
                         return $time = DateTimeHelper::diffFullPeriod($model->arrived, $model->departed);
                     }

                 }
            ],
            //'updated_at',
//            [
//                'class' => ActionColumn::className(),
//                'urlCreator' => function ($action, Records $model, $key, $index, $column) {
//                    return Url::toRoute([$action, 'id' => $model->id]);
//                 }
//            ],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end()?>
</div>
