<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Objects */

$this->title = 'Новый объект';
$this->params['breadcrumbs'][] = ['label' => 'Объекты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="objects-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
