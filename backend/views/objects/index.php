<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use  common\models\Objects;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ObjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Объекты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="objects-index">

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<?php \yii\widgets\Pjax::begin()?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'id',
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Поиск по id'
                ]

            ],
            [
                'attribute'=>'name',
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Поиск по названию'
                ]

            ],
            'address',
//            'position',
//            'status',
            //'created_at',
            //'updated_at',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Objects $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end()?>
</div>
