<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Objects */

$this->title = 'Редактирование объекта: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Объекты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="objects-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
