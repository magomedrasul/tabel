<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use nepster\basis\helpers\DateTimeHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Objects */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Оъекты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="objects-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы точно хотите удалить этот оъект?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'address',
            'position',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>
</div>


<hr>

<?php

$records = \common\models\Records::find()->andWhere(['objects_id'=>$model->id])->orderBy('id desc')->all();

$datesArrived1 = \yii\helpers\ArrayHelper::getColumn($records,'arrived');

$datesArrived = array_map(function ($item) {
    return date('d.m.Y',$item);
}, $datesArrived1);


$dateList = [];
foreach ($records as $item){
    $dateList[date('Y-m-d', $item->arrived)]['date'] =  date('Y,n-1,j', $item->arrived);
    $dateFormat = date('Y-m-d', $item->arrived);

    $events = \common\models\Records::find()->andWhere(['objects_id'=>$model->id])->andWhere(['date_format'=>date('d.m.Y', $item->arrived)]);

    $dateList[date('Y-m-d', $item->arrived)]['count'] = $events->count();
    $objects = \yii\helpers\ArrayHelper::getColumn($events->all(),'object');

    $str = '';

    foreach ($events->all() as $items){
        if($items->departed) {
            $time = DateTimeHelper::diffFullPeriod($items->arrived, $items->departed);
            $str = $str.'<li>'.$items->fio.' (Время на объекте: '.$time.')</li>';
        }else {
            $arrived = date('d.m.Y', $items->arrived);
            $str = $str.'<li>'.$items->fio.' (Прибытие'.$arrived.')</li>';
        }
    }


    $dateList[date('Y-m-d', $item->arrived)]['text'] =
        '<span><b>'.Yii::$app->formatter->asDate(date('Y-M-d', $item->arrived)).'</b><br></span>'.
        '<span><b>Прибытий на объекты: </b>('.$events->count().')</span>'.$str
    ;
}


?>


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<br>
<div id="calendar_basic" style="width: 100%; height: 300px;"></div>

<script type="text/javascript">
    google.charts.load("current", {packages: ["calendar"], language: 'ru'});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var dataTable = new google.visualization.DataTable();
        dataTable.addColumn({type: 'date', id: 'Date'});
        dataTable.addColumn({type: 'number', id: 'Won/Loss'});
        dataTable.addColumn({type: 'string', 'role': 'tooltip', 'p': {'html': true} });


        dataTable.addRows([

            <?php foreach ($dateList as $date) { ?>
            [new Date(<?=$date['date']?>), <?=$date['count']?>,'<div style="min-width:350px;padding: 5px; color: #0b2e13; font-size: 14px"><?=$date['text']?></div>'],
            <?php } ?>
        ]);

        var chart = new google.visualization.Calendar(document.getElementById('calendar_basic'));

        var options = {
            title: "График посещений",
            height: 300,
            tooltip: {isHtml: true},
            trigger: 'both',
            colorAxis: {minValue: 0, maxValue: <?=$model->getUsers()->count()?>, colors: ['#FF0000', '#00FF00']},
            calendar: {

                focusedCellColor: {
                    stroke: 'red',
                    strokeOpacity: 0.8,
                    strokeWidth: 3
                },
                cellSize: 20,
            },

            noDataPattern: {
                backgroundColor: '#FF0000',
                color: '#fff'
            }

        };
        chart.draw(dataTable, options);
    }

</script>




<?php

$records = \common\models\Records::find()->andWhere(['objects_id'=>$model->id])->orderBy('id desc')->all();

$dateList = [];
foreach ($records as $item){
    $dateFormat = date('Y-m-d', $item->arrived);

    $countEvents = \common\models\Records::find()->andWhere(['objects_id'=>$model->id])->andWhere(['date_format'=>date('d.m.Y', $item->arrived)])->count();

    $dateList[]=['title'=>'Количество событий: '.$countEvents,'date'=>$dateFormat,"badge"=>false];
}


?>
<script
        src="https://code.jquery.com/jquery-3.6.0.js"
        integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
<style>

    div.zabuto_calendar .table tr td.event div.day{

        background-color: #57B657;
        color: white;
    }

</style>
<script>
    var eventData = <?php echo json_encode($dateList) ?>;
    $(document).ready(function () {
        $(".calendar").zabuto_calendar({
            cell_border: true,
            today: true,
            language: "ru",
            data: eventData,
            show_days: true,
            weekstartson: 1,
            nav_icon: {
                prev: '<i class="icon-arrow-left"></i>',
                next: '<i class="icon-arrow-right"></i>'
            },
            action: function() {
                if (this.className.indexOf('event') > -1) {
                    loadDay(this.id);
                    $('.calendarEventsModalTitle').html("");
                    $('.calendarEventsModalBody').html("");
                }
            }
        });

        function loadDay(id) {
            id = id.substring(id.length - 10, id.length);
            id = (new Date(id)).toLocaleDateString();
            var object_id = <?=$model->id?>;
            $.ajax({
                url: '/backend/calendar/objects',
                cache: false,
                data: { id: id, object_id: object_id, },
                type: "get",
                success: function (data) {
                    $('div.desc-events .title').html("События на " + id);
                    $('div.desc-events').show();
                    $('div.desc-events .body').html(data);
                    $('div#loading').hide();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        };
    });
</script>
<!--<div class="container-fluid">-->
<!--    <br><br><br>-->
<!--    <div class="row">-->
<!--        <h3 class="text-center" style="margin-left: 25px ">График событий</h3><br><BR>-->
<!--        <div class="col-lg-8">-->
<!--            <div class="calendar">-->
<!--                <div class="desc-events" style="display:none;background-color: aliceblue;padding: 20px;border-radius: 10px;">-->
<!--                    <br>-->
<!--                    <h4 class="title" style="font-size: 17px"></h4>-->
<!--                    <br>-->
<!--                    <div class="body" style="font-size: 16px;">-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!--        </div>-->
<!--    </div>-->
<!---->
<!--</div>-->
