<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

           'id',
            [
                'attribute'=>'fio',
            ],
            'username',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            //'email:email',

            //'created_at',
            //'updated_at',
            //'verification_token',
            //'fio',

            [
                'attribute' => 'role',
                'label'=>'Роль',
                'format'=>'html',
                'value'=>function($data)
                {
                    if($data->role == \common\models\User::ROLE_ADMIN)
                    {
                        return '<div class="badge badge-success">Администратор</div>';
                    }

                    if($data->role ==\common\models\User::ROLE_USER)
                    {
                        return '<div class="badge badge-warning">Пользователь</div>';
                    }
                }
            ],
            [
                'attribute' => 'status',
                'format'=>'html',
                'value'=>function($data)
                {
                    if($data->status == \common\models\User::STATUS_ACTIVE)
                    {

                        return '<div class="badge badge-success">Активен</div>';
                    }

                    if($data->status == \common\models\User::STATUS_INACTIVE)
                    {
                        return '<div class="badge badge-danger">Не активен</div>';
                    }
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


