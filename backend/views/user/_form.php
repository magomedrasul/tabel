<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'username')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?php if ($model->isNewRecord) { ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
            <?php } else { ?>
                <?= $form->field($model, 'password')->textInput()->label('Новый пароль') ?>
            <?php } ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?=$form->field($model, 'role')->dropDownList([
               \common\models\User::ROLE_USER => 'Пользователь',
                \common\models\User::ROLE_ADMIN => 'Администратор',
            ]);?>
        </div>
        <div class="col-md-4">
            <?=$form->field($model, 'status')->dropDownList([
                \common\models\User::STATUS_ACTIVE=> 'Активен',
                \common\models\User::STATUS_INACTIVE => 'Не активен',
            ]);?>
        </div>
    </div>

    <hr>
    <br>
    <h3 class="text-center">Объекты для пользователя</h3>

    <div class="row">

        <div class="col-md-12">
            <?php

            $data = \common\models\Objects::find()->andWhere(['status' => 1])->all();
            $data = ArrayHelper::map($data, 'id', 'name');
            ?>
            <?= $form->field($model, 'objects_arr')->widget(Select2::classname(), [
                'data' => $data,
                'options' => ['placeholder' => 'Выберите объекты ...', 'multiple' => true],
                'pluginOptions' => [
                    'tags' => true,
                    'tokenSeparators' => [',', ' '],
                    'allowClear' => true,
                    'maximumInputLength' => 1000
                ],
            ])->label('Объекты'); ?>

        </div>
    </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
