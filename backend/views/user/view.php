<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use tecnocen\yearcalendar\widgets\Calendar;
use yii\web\JsExpression;
use yii\db\Expression;
use nepster\basis\helpers\DateTimeHelper;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->getUsername();
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>


    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы точно хотите удалить данного пользователя?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

<div class="row">
    <div class="col-md-6">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            [
                'attribute'=>'fio',
            ],
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
//            'email:email',
            [
                'attribute' => 'role',
                'label'=>'Роль',
                'format'=>'html',
                'value'=>function($data)
                {
                    if($data->role == \common\models\User::ROLE_ADMIN)
                    {
                        return '<div class="badge badge-success">Администратор</div>';
                    }

                    if($data->role ==\common\models\User::ROLE_USER)
                    {
                        return '<div class="badge badge-warning">Пользователь</div>';
                    }
                }
            ],
            [
                'attribute' => 'status',
                'format'=>'html',
                'value'=>function($data)
                {
                    if($data->status == \common\models\User::STATUS_ACTIVE)
                    {

                        return '<div class="badge badge-success">Активен</div>';
                    }

                    if($data->status == \common\models\User::STATUS_INACTIVE)
                    {
                        return '<div class="badge badge-danger">Не активен</div>';
                    }
                }
            ],
            'created_at:datetime',
            'updated_at:datetime',

        ],
    ]) ?>
    </div>


    <style>

        div.zabuto_calendar .table tr td.event div.day{

            background-color: #57B657;
            color: white;
        }

    </style>
    <div class="col-md-6">
        <?php if($model->getObjects()->all()) {?>
                    <h2 >Объекты для этого пользователя</h2>
                    <hr>
                    <?php
                    $objects=$model->getObjects();
                    ?>

                    <?php foreach( $objects->all() as $item) { ?>
                        <h4><a href="<?=\yii\helpers\Url::to(['/objects/view','id'=>$item->id])?>"><?=$item->name?></h4></a>
                    <?php } ?>
        <?php }?>
    </div>
</div>
<hr>

<?php

$records = \common\models\Records::find()->andWhere(['user_id'=>$model->getId()])->orderBy('id desc')->all();

$datesArrived1 = \yii\helpers\ArrayHelper::getColumn($records,'arrived');

$datesArrived = array_map(function ($item) {
    return date('d.m.Y',$item);
}, $datesArrived1);


$dateList = [];
foreach ($records as $item){
    $dateList[date('Y-m-d', $item->arrived)]['date'] =  date('Y,n-1,j', $item->arrived);
    $dateFormat = date('Y-m-d', $item->arrived);

    $events = \common\models\Records::find()->andWhere(['user_id'=>$model->getId()])->andWhere(['date_format'=>date('d.m.Y', $item->arrived)]);

    $dateList[date('Y-m-d', $item->arrived)]['count'] = $events->count();
    $objects = \yii\helpers\ArrayHelper::getColumn($events->all(),'object');

    $str = '';

    foreach ($events->all() as $items){
        if($items->departed) {
            $time = DateTimeHelper::diffFullPeriod($items->arrived, $items->departed);
            $str = $str.'<li>'.$items->object.' (Время на объекте: '.$time.')</li>';
        }else {
            $arrived = date('d.m.Y', $items->arrived);
            $str = $str.'<li>'.$items->object.' (Прибытие'.$arrived.')</li>';
        }
    }


    $dateList[date('Y-m-d', $item->arrived)]['text'] =
        '<span><b>'.Yii::$app->formatter->asDate(date('Y-M-d', $item->arrived)).'</b><br></span>'.
        '<span><b>Прибытий на объекты: </b>('.$events->count().')</span>'.$str
    ;
}


?>


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<br>
<div id="calendar_basic" style="width: 100%; height: 300px;"></div>

<script type="text/javascript">
    google.charts.load("current", {packages: ["calendar"], language: 'ru'});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var dataTable = new google.visualization.DataTable();
        dataTable.addColumn({type: 'date', id: 'Date'});
        dataTable.addColumn({type: 'number', id: 'Won/Loss'});
        dataTable.addColumn({type: 'string', 'role': 'tooltip', 'p': {'html': true} });


        dataTable.addRows([

            <?php foreach ($dateList as $date) { ?>
            [new Date(<?=$date['date']?>), <?=$date['count']?>,'<div style="min-width:350px;padding: 5px; color: #0b2e13; font-size: 14px"><?=$date['text']?></div>'],
          <?php } ?>
        ]);

        var chart = new google.visualization.Calendar(document.getElementById('calendar_basic'));

        var options = {
            title: "График посещений",
            height: 300,
            tooltip: {isHtml: true},
            trigger: 'both',
            colorAxis: {minValue: 0, maxValue: <?=$model->getObjects()->count()?>, colors: ['#FF0000', '#00FF00']},
            calendar: {

                focusedCellColor: {
                    stroke: 'red',
                    strokeOpacity: 0.8,
                    strokeWidth: 3
                },
                cellSize: 20,
            },

            noDataPattern: {
                backgroundColor: '#FF0000',
                color: '#fff'
            }

        };
        chart.draw(dataTable, options);
    }

</script>