<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use tecnocen\yearcalendar\widgets\Calendar;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->getUsername();
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>


    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы точно хотите удалить данного пользователя?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

<div class="row">
    <div class="col-md-6">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            [
                'attribute'=>'fio',
            ],
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
//            'email:email',
            [
                'attribute' => 'role',
                'label'=>'Роль',
                'format'=>'html',
                'value'=>function($data)
                {
                    if($data->role == \common\models\User::ROLE_ADMIN)
                    {
                        return '<div class="badge badge-success">Администратор</div>';
                    }

                    if($data->role ==\common\models\User::ROLE_USER)
                    {
                        return '<div class="badge badge-warning">Пользователь</div>';
                    }
                }
            ],
            [
                'attribute' => 'status',
                'format'=>'html',
                'value'=>function($data)
                {
                    if($data->status == \common\models\User::STATUS_ACTIVE)
                    {

                        return '<div class="badge badge-success">Активен</div>';
                    }

                    if($data->status == \common\models\User::STATUS_INACTIVE)
                    {
                        return '<div class="badge badge-danger">Не активен</div>';
                    }
                }
            ],
            'created_at:datetime',
            'updated_at:datetime',

        ],
    ]) ?>
    </div>


    <style>

        div.zabuto_calendar .table tr td.event div.day{

            background-color: #57B657;
            color: white;
        }

    </style>
    <div class="col-md-6">
        <?php if($model->getObjects()->all()) {?>
                    <h2 >Объекты для этого пользователя</h2>
                    <hr>
                    <?php
                    $objects=$model->getObjects();
                    ?>

                    <?php foreach( $objects->all() as $item) { ?>
                        <h4><a href="<?=\yii\helpers\Url::to(['/objects/view','id'=>$item->id])?>"><?=$item->name?></h4></a>
                    <?php } ?>
        <?php }?>
    </div>
</div>
<hr>

<?php

$records = \common\models\Records::find()->andWhere(['user_id'=>$model->getId()])->orderBy('id desc')->all();

$dateList = [];
foreach ($records as $item){
    $dateFormat = date('Y-m-d', $item->arrived);

    $countEvents = \common\models\Records::find()->andWhere(['user_id'=>$model->getId()])->andWhere(['date_format'=>date('d.m.Y', $item->arrived)])->count();

    $dateList[]=['title'=>'Количество событий: '.$countEvents,'date'=>$dateFormat,"badge"=>false];
}


?>
<script
        src="https://code.jquery.com/jquery-3.6.0.js"
        integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>

<script>
    var eventData = <?php echo json_encode($dateList) ?>;
    $(document).ready(function () {
        $(".calendar").zabuto_calendar({
            cell_border: true,
            today: true,
            language: "ru",
            data: eventData,
            show_days: true,
            weekstartson: 1,
            nav_icon: {
                prev: '<i class="icon-arrow-left"></i>',
                next: '<i class="icon-arrow-right"></i>'
            },
            action: function() {
                if (this.className.indexOf('event') > -1) {
                    loadDay(this.id);
                    $('.calendarEventsModalTitle').html("");
                    $('.calendarEventsModalBody').html("");
                }
            }
        });

        function loadDay(id) {
            id = id.substring(id.length - 10, id.length);
            id = (new Date(id)).toLocaleDateString();
            var user_id = <?=$model->getId()?>;
            $.ajax({
                url: '/backend/calendar/events',
                cache: false,
                data: { id: id, user_id: user_id, },
                type: "get",
                success: function (data) {
                    $('div.desc-events .title').html("События на " + id);
                    $('div.desc-events').show();
                    $('div.desc-events .body').html(data);
                    $('div#loading').hide();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        };
    });
</script>
<br><br>

<!--<div class="container-fluid">-->
<!--<div class="row">-->
<!--    <h3 class="text-center" style="margin-left: 25px ">График событий</h3><br><BR>-->
<!--    <div class="col-lg-8">-->
<!--        <div class="calendar">-->
<!--            <div class="desc-events" style="display:none;background-color: aliceblue;padding: 20px;border-radius: 10px;">-->
<!--                <br>-->
<!--                <h4 class="title" style="font-size: 17px"></h4>-->
<!--                <br>-->
<!--                <div class="body" style="font-size: 16px;">-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!---->
<!--    </div>-->
<!--</div>-->
<!---->
<!--</div>-->