<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth px-0">
            <div class="row w-100 mx-0">
                <div class="col-lg-4 mx-auto">
                    <div class="auth-form-light text-left py-5 px-4 px-sm-5">
                        <div class="brand-logo text-center">
                            <h3><?=Yii::$app->name?> - Админ панель</h3>
                        </div>
                        <h4 class="text-center">Пожалуйста, авторизуйтесь</h4>
                        <br>
                        <?php $form = ActiveForm::begin(['class' => 'pt-3']); ?>
                        <form class="pt-3">
                            <div class="form-group">
                                <?= $form->field($model, 'username')->textInput(['class' => 'form-control form-control-lg','placeholder'=>'Ваш Логин'])->label(false) ?>
                            </div>
                            <div class="form-group">
                                <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control form-control-lg','placeholder'=>'Ваш пароль'])->label(false) ?>
                            </div>

                            <div class="mb-4">
                                <div class="form-check">
                                    <label class="form-check-label text-muted">
                                        <?= $form->field($model, 'rememberMe')->checkbox()->label()->label('Запомнить меня') ?>
                                    </label>
                                    <i class="input-helper"></i>
                                </div>
                            </div>

                            <div class="mt-3">
                                <style>
                                    button {
                                        width: 100%;
                                        background: #0095FF !important;
                                    }
                                </style>
                                <?= Html::submitButton('Войти', ['class' => 'btn btn-success', 'name' => 'login-button']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
    </div>
</div>



