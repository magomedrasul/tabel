<?php

/* @var $this \yii\web\View */
/* @var $content string */

use common\widgets\Alert;
use backend\assets\AppAsset;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>


    <style>

        table th{
            color: #687C97 !important;
        }
        .help-block{

            color: red;
        }

        .form-group label{

            font-weight: 600;
        }

        h2, .h2 {
            font-size: 22px !important;
        }

        .table th, .jsgrid .jsgrid-table th, .table td, .jsgrid .jsgrid-table td {
            white-space: inherit !important;
        }

        .table svg{
            width: 15px !important;
            height: 15px !important;
        }

    </style>

    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>

</head>

<body>
<?php $this->beginBody() ?>
<div class="container-scroller">
    <?=$this->render('navbar')?>
    <div class="container-fluid page-body-wrapper">
        <?=$this->render('sidebar')?>
        <div class="main-panel">
            <div class="content-wrapper">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">

                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">
                                    <h2><?=Html::encode($this->title)?></h2>
                                </h4>
                                <hr>
                                <div class="table-responsive pt-3">
                                    <?= $content ?>
                                    <br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $this->endBody() ?>
<script>
    $(document).ready(function(){

        $(".edit-app").on("click", function(){
            var id = $(this).attr('data-id');
            var value = $(this).attr('data-val');

            if(value.length>0){
                $.ajax({
                    url: '/applications/set-status',
                    type: 'get',
                    dataType: "json",
                    data: {
                        id: id,
                        value: value,
                    },
                    success: function(data) {

                        if(data.status==true) {
                            location.reload();
                       }
                    },
                });
            }
            return false;
        });


        $(".warr-sn input").on('keyup paste', function(){
            var sn = $(this).val();
            if(sn.length>6){
                $.ajax({
                    url: '/products/get-product-by-sn',
                    type: 'get',
                    dataType: "json",
                    data: {
                        sn: sn,
                    },
                    success: function(data) {
                        if(data.status==true){
                            $(".warr-title input").val(data.name);
                            $(".warr-title input[type=hidden]").val(data.period);
                        }
                    },
                });
            }
        });

    });
</script>

</body>
</html>
<?php $this->endPage();
