<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item <?php if(Yii::$app->controller->id=='objects'){?> active <?php } ?>">
            <a class="nav-link" href="<?=\yii\helpers\Url::to(['/objects/index'])?>">
                <i class="icon-paper menu-icon"></i>
                <span class="menu-title">Объекты</span>
            </a>
        </li>
        <li class="nav-item <?php if(Yii::$app->controller->id=='user'){?> active <?php } ?>">
            <a class="nav-link" href="<?=\yii\helpers\Url::to(['/user/index'])?>">
                <i class="icon-paper menu-icon"></i>
                <span class="menu-title">Пользователи</span>
            </a>
        </li>

        <li class="nav-item <?php if(Yii::$app->controller->id=='records'){?> active <?php } ?>">
            <a class="nav-link" href="<?=\yii\helpers\Url::to(['/records/index'])?>">
                <i class="icon-paper menu-icon"></i>
                <span class="menu-title">Записи</span>
            </a>
        </li>
    </ul>
</nav>