<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
      'vendors/feather/feather.css',
        'vendors/ti-icons/css/themify-icons.css',
       'vendors/css/vendor.bundle.base.css',
        'vendors/datatables.net-bs4/dataTables.bootstrap4.css',
        'vendors/ti-icons/css/themify-icons.css',
        //'text/css" href="js/select.dataTables.min.css',
        'css/vertical-layout-light/style.css',
        'css/zabuto_calendar.min.css',
        'http://garant-guard.ru/urv/css/tuning.css'
    ];
    public $js = [
        'js/dataTables.select.min.js',
        'js/off-canvas.js',
        'js/hoverable-collapse.js',
        'js/Chart.min.js',
        'js/owl-carousel.js',
//        'js/zabuto_calendar.min.js',
        //'js/chart.js',
//        'js/chartist.js',
//        'js/dashboard.js',
//        'js/hoverable-collapse.js',
//        'js/Chart.roundedBarCharts.js'
    ];


    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];

}
