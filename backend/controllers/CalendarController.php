<?php


namespace backend\controllers;


use common\models\Records;
use yii\web\Controller;

class CalendarController extends Controller
{
    public function actionEvents(){

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $date = \Yii::$app->request->get('id');
        $user = \Yii::$app->request->get('user_id');
        $records = \common\models\Records::find()->andWhere(['user_id'=>$user])->andWhere(['like','date_format',$date])->orderBy('id ASC')->all();

        $res = [];

        foreach ($records as $item){


         $str =  '<p style="display:inline-block;margin-top:-15px;"><b>Объект: </b>'.$item->object.' - <b>Прибытие: </b>'.date('d.m.Y H:i:s',$item->arrived);

         if(!empty($item->departed)){

             $time = floor((intval($item->departed) - intval($item->arrived)) / 60);

             if ($time < 59) {
                 if($time<1){
                     $time =  floor((intval($item->departed) - intval($item->arrived))) . ' Сек.';
                 }else{
                     $time =  $time . ' Мин.';
                 }

             } else {
                 $hours = floor($time / 60);
                 $minutes = $time % 60;

                 $time =  $hours . ' Час ' . $minutes . ' Мин.';
             }


             $str.='<b> - Убытие: </b>'.date('d.m.Y H:i:s',$item->departed).' - <b>Время на объекте: </b>'. $time.'</p>';
         }

$res [] = $str.'<br>';

        }

        return $res;
    }


    public function actionObjects(){


        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $date = \Yii::$app->request->get('id');
        $object = \Yii::$app->request->get('object_id');
        $records = \common\models\Records::find()->andWhere(['objects_id'=>$object])->andWhere(['like','date_format',$date])->orderBy('id ASC')->all();

        $res = [];

        foreach ($records as $item){


            $str =  '<p style="display:inline-block;margin-top:-15px;"><b>Сотрудник: </b>'.$item->fio.' - <b>Прибытие: </b>'.date('d.m.Y H:i:s',$item->arrived);

            if(!empty($item->departed)){

                $time = floor((intval($item->departed) - intval($item->arrived)) / 60);

                if ($time < 59) {
                    if($time<1){
                        $time =  floor((intval($item->departed) - intval($item->arrived))) . ' Сек.';
                    }else{
                        $time =  $time . ' Мин.';
                    }

                } else {
                    $hours = floor($time / 60);
                    $minutes = $time % 60;

                    $time =  $hours . ' Час ' . $minutes . ' Мин.';
                }


                $str.='<b> - Убытие: </b>'.date('d.m.Y H:i:s',$item->departed).' - <b>Время на объекте: </b>'. $time.'</p>';
            }

            $res [] = $str.'<br>';

        }

        return $res;
    }
}