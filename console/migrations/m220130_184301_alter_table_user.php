<?php

use yii\db\Migration;

/**
 * Class m220130_184301_alter_table_user
 */
class m220130_184301_alter_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'role', $this->integer());
        $this->addColumn('{{%user}}', 'fio', $this->string());

        $this->createAdmin();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'role');
        $this->dropColumn('{{%user}}', 'fio');
    }

    public function createAdmin()
    {
        $this->insert('{{%user}}',[
            'email' => 'admin@yandex.ru',
            'password_hash' => Yii::$app->security->generatePasswordHash('Qwer1234'),
            'status'=>\common\models\User::STATUS_ACTIVE,
            'role'=>\common\models\User::ROLE_ADMIN,
            'fio'=>'Гусейнов Расул',
            'username'=>'admin',
            'auth_key' => Yii::$app->security->generateRandomString(),
            'created_at'=>$time=time(),
            'updated_at'=>$time,
            'verification_token' => Yii::$app->security->generateRandomString() . '_' . time()
        ]);
    }
}
