<?php

use yii\db\Migration;

/**
 * Class m220316_131626_alter_table_objects
 */
class m220316_131626_alter_table_objects extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%records}}', 'arrived', $this->integer());
        $this->addColumn('{{%records}}', 'departed', $this->integer());
    }
}
