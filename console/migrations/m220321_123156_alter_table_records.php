<?php

use yii\db\Migration;

/**
 * Class m220321_123156_alter_table_records
 */
class m220321_123156_alter_table_records extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%records}}', 'date_format', $this->string());
        $this->addColumn('{{%records}}', 'time_on_object', $this->string());
    }

}
