<?php

use yii\db\Migration;

/**
 * Class m220316_113942_alter_table_objects
 */
class m220316_113942_alter_table_records extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%records}}', 'objects_id', $this->integer());
    }
}
