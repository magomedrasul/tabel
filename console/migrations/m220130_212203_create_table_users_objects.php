<?php

use yii\db\Migration;

/**
 * Class m220130_212203_create_table_users_objects
 */
class m220130_212203_create_table_users_objects extends Migration
{
    public function up()
    {

        $this->createTable('{{%users_objects}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'object_id' => $this->integer()->notNull(),
        ]);


        // creates index for column `user_id`
        $this->createIndex(
            'idx_user_id',
            'users_objects',
            'user_id'
        );


        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_id',
            'users_objects',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx_object_id',
            'users_objects',
            'object_id'
        );


        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-object_id',
            'users_objects',
            'object_id',
            'objects',
            'id',
            'CASCADE'
        );

    }
}
