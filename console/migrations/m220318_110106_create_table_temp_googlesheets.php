<?php

use yii\db\Migration;

/**
 * Class m220318_110106_create_table_temp_googlesheets
 */
class m220318_110106_create_table_temp_googlesheets extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%temp_googlesheets}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->unique(),
            'range'=>$this->string()->notNull(),
            'position'=>$this->integer()->defaultValue(100),
            'status'=>$this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%temp_googlesheets}}');
    }
}
