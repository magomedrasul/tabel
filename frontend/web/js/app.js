$(document).ready(function () {
    $("table tr .arrived ").on("click", function () {
        var el = $(this);
        var id = $(this).parent('td').parent('tr').attr('data-id');
        if (id) {
            $.ajax({
                url: '/records/save',
                type: 'post',
                dataType: "json",
                data: {
                    id: id,
                },
                success: function (data) {
                    if (data.status == true) {
                        $('.ok' + id).css('display', 'inline');
                        el.attr("disabled", "disabled");
                        $('.arrived').attr("disabled", "disabled");
                        el.next('.departed').attr("disabled", false);
                    }
                },
            });
        }
        return false;
    });

    $("table tr .departed ").on("click", function () {
        var el = $(this);
        var id = $(this).parent('td').parent('tr').attr('data-id');
        var type = 2;
        if (id) {
            $.ajax({
                url: '/records/save',
                type: 'post',
                dataType: "json",
                data: {
                    id: id,
                    type: type
                },
                success: function (data) {
                    if (data.status == true) {
                        $('.ok' + id).css('display', 'inline');
                        el.prev('button').toggleClass('today_arrived');
                        el.prev('button').attr("disabled", 'disabled');
                        el.attr("disabled", 'disabled');
                        $('.arrived').attr("disabled", false);
                        $('.today_arrived').attr("disabled",'disabled');
                    }
                },
            });
        }
        return false;
    });
});



