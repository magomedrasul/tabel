<?php


namespace frontend\controllers;


use common\models\Objects;
use common\models\Records;
use yii\web\Controller;
use Yii;
use nepster\basis\helpers\DateTimeHelper;

class RecordsController extends Controller
{

    public function actionSave()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($_POST['id'] and !isset($_POST['type'])) {
            $id = intval($_POST['id']);
            $ob = Objects::findOne($id);
            if ($ob) {
                $record = new Records();
                $record->user_id = \Yii::$app->user->getId();
                $record->fio = \Yii::$app->user->identity->getUsername();
                $record->object = $ob->name;
                $record->objects_id = $id;
                $record->arrived = time();
                if ($record->save()) {
                    $data = [$record->user_id, $record->fio,  $record->object, date('d.m.Y H:i:s', $record->arrived),' ',' '];
                    $res = Yii::$app->googleSheets->add($data,true);
                    return ['status' => true];
                }else{
                    return $record->getErrors();
                }
            }
        }else{
            $id = intval($_POST['id']);
            $ob = Objects::findOne($id);

            $record = Records::find()
                ->andWhere(['user_id'=>Yii::$app->user->getId()])
                ->andWhere(['objects_id'=>$ob->id])
                ->andWhere(['departed'=>null])
                ->orderBy('id desc')
                ->one();
            if ($record) {
                $record->departed = time();
                $record->time_on_object = $record->departed - $record->arrived;
                $record->date_format = date('d.m.Y',$record->departed);
                if ($record->save(false,['departed','time_on_object','date_format'])) {

                    if($record->departed) {
                        $time = DateTimeHelper::diffFullPeriod($record->arrived, $record->departed);
                        $data = [$record->user_id, $record->fio,  $record->object, date('d.m.Y H:i:s', $record->arrived), date('d.m.Y H:i:s', $record->departed),$time];
                        $res = Yii::$app->googleSheets->add($data,false);
                        return ['status' => true];
                    }
                }else{
                    return $record->getErrors();
                }
            }
        }
    }
}