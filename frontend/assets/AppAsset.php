<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'vendors/feather/feather.css',
        'vendors/ti-icons/css/themify-icons.css',
        'vendors/css/vendor.bundle.base.css',
//        'vendors/datatables.net-bs4/dataTables.bootstrap4.css',
//        'vendors/ti-icons/css/themify-icons.css',
//        'text/css" href="js/select.dataTables.min.css',
        'css/vertical-layout-light/style.css',
        'http://garant-guard.ru/urv/css/tuning.css'
    ];
    public $js = [
        'js/dataTables.select.min.js',
        'js/off-canvas.js',
        'js/hoverable-collapse.js',
        'js/app.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}