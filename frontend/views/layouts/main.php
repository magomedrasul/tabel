<?php

/* @var $this \yii\web\View */

/* @var $content string */

use common\widgets\Alert;
use frontend\assets\AppAsset;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>" class="h-100">
    <head>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

        <style>
            .sidebar .nav .nav-item.active > .nav-link {
                background: #0095FF !important;
            }

            .fa-check-circle {

                font-size: 20px;
                margin-left: 5px;
                color: green;
            }

            .sidebar .nav:not(.sub-menu) > .nav-item:hover > .nav-link, .sidebar .nav:not(.sub-menu) > .nav-item:hover[aria-expanded="true"] {

                background: #0095FF !important;
            }

            svg path {

                fill: #687C97;
            }

        </style>
        <style>

            table th {
                color: #687C97 !important;
            }

            .help-block {

                color: red;
            }

            .form-group label {

                font-weight: 600;
            }

            h2, .h2 {
                font-size: 22px !important;
            }

            .table th, .jsgrid .jsgrid-table th, .table td, .jsgrid .jsgrid-table td {
                white-space: inherit !important;
            }

            .table svg {

                width: 15px !important;
                height: 15px !important;
            }

        </style>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>

    <body>
    <?php $this->beginBody() ?>
    <div class="container-scroller">
        <?= $this->render('navbar') ?>
        <div class="container-fluid page-body-wrapper">

            <div class="main-panel" style="width: 100%">
                <div class="content-wrapper">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    <div class="row">
                        <div class="col-lg-12 grid-margin stretch-card">

                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">

                                        <h2><?php if (!Yii::$app->user->isGuest) {
                                                echo Yii::$app->user->identity->getUsername();
                                            } ?></h2>
                                    </h4>
                                    <hr>
                                    <div class="table-responsive pt-3">
                                        <?= $content ?>
                                        <br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage();
