<?php

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
?>
<?php if (Yii::$app->user->identity->getObjects()->all()) { ?>
        <h3 class="text-center">Объекты</h3>
    <br>
    <table class="table table-bordered table-hover">
        <th>Объект</th>
        <th>Действия</th>
        <tbody>
        <?php $i = 1;
        foreach (Yii::$app->user->identity->getObjects()->all() as $item) { ?>
                <?php $item->recordsTodayIsObjects()?>
            <tr data-id="<?= $item->id ?>">
                <td><?= $item->name ?></td>
                 <td>
                     <button class="btn btn-outline-success btn-sm arrived <?php if($item->recordsTodayIsObjects()){?>today_arrived<?php } ?>" <?php if($item->recordsTodayIsObjects()){?> disabled="disabled" <?php } ?> <?php if(\common\models\Objects::userIsArrivedToday()){?> disabled="disabled" <?php } ?>>Прибыл </button>
                     <button class="btn btn-outline-info btn-sm departed" <?php if(!$item->userIsDepartedToday()){?> disabled="disabled" <?php } ?>>Убыл </button>
                     <i class="fas fa-check-circle ok<?=$item->id?>" style=" cursor:default;display: none"></i>
                </td>
            </tr>
            <?php $i++;
        } ?>
        </tbody>
    </table>
<?php } ?>