<?php

namespace common\components;

use common\models\TempGooglesheets;
use yii\base\Component;

class GoogleSheets extends Component{

    public function add($data, $flag = false){

        $client = new \Google_Client();
        $client->setAuthConfig('key/rasul-339814-419ffaac816f.json'); // Use your own client_secret JSON file
        $client->addScope(\Google_Service_Sheets::SPREADSHEETS);
        $accessToken = 'ACCESS_TOKEN'; // Use your generated access token
        $client->setAccessToken($accessToken);
        $sheet_service = new \Google_Service_Sheets($client);

        // Set the sheet ID
        $fileId = '1kLboGKKHDEKDUuZnEZyonffcwRnSQ-fIozQC62NaHhg'; // Copy & paste from a spreadsheet URL
        $response = $sheet_service->spreadsheets->get($fileId);
        $pageTitle = $response->getSheets()[0]->getProperties()['title'];
        $range = $pageTitle;

        $values = [
            $data
        ];

        $ValueRange = new \Google_Service_Sheets_ValueRange();
        $ValueRange->setValues($values);
        $options = ['valueInputOption' => 'USER_ENTERED'];

        if($flag){
            $res = $sheet_service->spreadsheets_values->append($fileId, $range, $ValueRange, $options);
            $tempRange = TempGooglesheets::find()->andWhere(['user_id'=>\Yii::$app->user->getId()])->one();

            if($tempRange){
                $tempRange->range =$res['updates']['updatedRange'];
//                $tempRange->range =str_replace('"!', "", $tempRange->range);
                $tempRange->save('false',['range']);
            }else{
                $temp = new TempGooglesheets();
                $temp->user_id = \Yii::$app->user->getId();
                $temp->range =  $res['updates']['updatedRange'];
                $temp->save();
            }

        }else{
            $tempRange = TempGooglesheets::find()->andWhere(['user_id'=>\Yii::$app->user->getId()])->one();
            $ranges = $tempRange->range;
            $ranges = str_replace("'", '', $ranges);
            $res = $sheet_service->spreadsheets_values->update($fileId, $ranges, $ValueRange, $options);
        }

//        try {
//            // Execute the request
//            $response = $sheet_service->spreadsheets->batchUpdate($fileId, $batchUpdateRequest);
//            if( $response->valid() ) {
//                // Success, the row has been added//
//               return true;
//            }
//        } catch (\Exception $e) {
//            // Something went wrong
//            error_log($e->getMessage());
//        }
    }
}