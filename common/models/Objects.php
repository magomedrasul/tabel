<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use function MongoDB\BSON\toRelaxedExtendedJSON;

/**
 * This is the model class for table "objects".
 *
 * @property int $id
 * @property string $name
 * @property string|null $address
 * @property int|null $position
 * @property int|null $status
 * @property int $created_at
 * @property int $updated_at
 */
class Objects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objects';
    }


    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['name', 'unique', 'targetClass' => '\common\models\Objects', 'message' => 'Такой объект уже существует!'],
            [['name'], 'required'],
            [['position', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'address'], 'string', 'max' => 555],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название объекта',
            'address' => 'Адрес',
            'position' => 'Сортировка',
            'status' => 'Статус',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }


    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])
            ->viaTable('users_objects', ['object_id' => 'id']);
    }


    public function recordsTodayIsObjects()
    {
        $records = Records::find()
            ->andWhere(['user_id' => Yii::$app->user->getId()])
            ->andWhere(['objects_id' => $this->id])
            ->andWhere(['not', ['departed'=>null]])
            ->orderBy('id desc')
            ->one();

        if (!empty($records)) {
            if (date('d') == date('d', $records->arrived)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public static function userIsArrivedToday()
    {
        $records = Records::find()
            ->andWhere(['user_id' => Yii::$app->user->getId()])
            ->andWhere(['departed' => null])
            ->orderBy('id desc')
            ->one();

        if (!empty($records)) {
            if (date('d') == date('d', $records->arrived)) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }


    public function userIsDepartedToday()
    {
        $records = Records::find()
            ->andWhere(['user_id' => Yii::$app->user->getId()])
            ->andWhere(['objects_id' => $this->id])
            ->andWhere(['departed' => null])
            ->orderBy('id desc')
            ->one();

        if (!empty($records)) {
            if (date('d') == date('d', $records->arrived)) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }


    public function userIsArrived()
    {
        if (Records::find()
            ->andWhere(['user_id' => Yii::$app->user->getId()])
            ->andWhere(['objects_id' => $this->id])
            ->exists()) {
            return true;
        }
        return false;
    }

}
