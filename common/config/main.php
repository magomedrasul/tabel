<?php
return [
    'name'=>'УРВ',//Учет рабочего времени
     'language'=>'ru_RU',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'googleSheets'=>[
            'class' => 'common\components\GoogleSheets',
        ],
    ],
];
